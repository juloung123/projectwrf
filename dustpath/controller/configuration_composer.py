import datetime
import asyncio
import json
import jinja2
import logging
import os
logger = logging.getLogger(__name__)
import pathlib
from dustpath import models


class configuration_composer:
    def __init__(self):
        self.hello = 'hello world'
        
    def test(self):
        config_parameters = models.Project.objects()
        print(len(config_parameters))

    def get_namelist_input(self):
            template_file = "dustpath/web/static/jinjaform/namelistinput.j2"
            #json_parameter_file = "parameters.json"
            output_directory = "dustpath/web/static/namelist"

            # read the contents from the JSON files
            print("Read JSON parameter file...")
            config_parameters = models.Project.objects()
            print(config_parameters)
            #config_parameters = json.load(open(json_parameter_file))

            # next we need to create the central Jinja2 environment and we will load
            # the Jinja2 template file (the two parameters ensure a clean output in the
            # configuration file)
            print(pathlib.Path.cwd())
            print("Create Jinja2 environment...")
            env = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath="."),
            #(loader=jinja2.FileSystemLoader(searchpath="."),
                                    trim_blocks=True,
                                    lstrip_blocks=True)
            template = env.get_template(template_file)

            # we will make sure that the output directory exists
            if not os.path.exists(output_directory):
                os.mkdir(output_directory)

            # now create the templates
            print("Create templates...")
            # result = template.render(config_parameters)
            # f = open(os.path.join(output_directory,  "test.input" ), "w")
            # f.write(result)
            # f.close()
            # print("Configuration created... test.input")    
            for parameter in config_parameters:
                result = template.render(wrf_config=parameter.wrf_config)
                f = open(os.path.join(output_directory, parameter['name'] + ".input" ), "w")
                f.write(result)
                f.close()
                print("Configuration '%s' created..." % (parameter['name']+ ".input"))
            print("DONE")        
            
    def get_namelist_wps(self):
            template_file = "dustpath/web/static/jinjaform/namelistwps.j2"
            #json_parameter_file = "parameters.json"
            output_directory = "dustpath/web/static/namelist"

            # read the contents from the JSON files
            print("Read JSON parameter file...")
            config_parameters = models.Project.objects()
            print(config_parameters)
            #config_parameters = json.load(open(json_parameter_file))

            # next we need to create the central Jinja2 environment and we will load
            # the Jinja2 template file (the two parameters ensure a clean output in the
            # configuration file)
            print(pathlib.Path.cwd())
            print("Create Jinja2 environment...")
            env = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath="."),
            #(loader=jinja2.FileSystemLoader(searchpath="."),
                                    trim_blocks=True,
                                    lstrip_blocks=True)
            template = env.get_template(template_file)

            # we will make sure that the output directory exists
            if not os.path.exists(output_directory):
                os.mkdir(output_directory)

            # now create the templates
            print("Create templates...")
            # result = template.render(config_parameters)
            # f = open(os.path.join(output_directory,  "test.input" ), "w")
            # f.write(result)
            # f.close()
            # print("Configuration created... test.input")    
            for parameter in config_parameters:
                result = template.render(wrf_config=parameter.wrf_config)
                f = open(os.path.join(output_directory, parameter['name'] + ".wps" ), "w")
                f.write(result)
                f.close()
                print("Configuration '%s' created..." % (parameter['name']+ ".wps"))
            print("DONE")     